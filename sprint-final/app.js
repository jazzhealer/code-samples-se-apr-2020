import express from "express";
import BluebirdPromise from "bluebird";
import { PORT_TO_USE, uploader, staticDirectory, uploadDirectory } from "./helpers";
import { registerRoutes } from "./routes";
import mongoose from "mongoose";
import "./db";

const startServer = async () => {
  // you can omit the next line but maybe you need it for Kapstone? ;)
  mongoose.Promise = BluebirdPromise;
  await mongoose.connect(`${process.env.MONGO_URL}/${process.env.MONGO_DB_NAME}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  const app = express();
  // middleware
  app.use(express.json()); // tell xpress that we care about incoming JSON in the req
  app.use(express.urlencoded({ extended: false })); // tell xpress that we care about incoming urlencodeddata in the req
  app.use(express.static(staticDirectory));

  // app.use((req, res, next) => {
  //   const authHeader = req.headers.authorization;

  //   if (!authHeader || authHeader !== AUTH_TOKEN) {
  //     res.status(401).send("You dont belong");
  //     return;
  //   }
  //   next();
  // });

  registerRoutes(app);

  app.post("/upload", uploader.single("pic"), (req, res) => {
    res.send(req.file.filename);
  });

  app.listen(PORT_TO_USE, () => {
    console.log(`Server successfully running on port ${PORT_TO_USE}`);
  });
};

try {
  startServer();
} catch (err) {
  console.error("Server was unable to start", err);
}

process.on("unhandledRejection", (whyDidThePromiseGetRejected, theActualPromiseThatGotRejected) => {
  console.error("Unhandled rejection at: ", theActualPromiseThatGotRejected, "reason:", whyDidThePromiseGetRejected);
});
