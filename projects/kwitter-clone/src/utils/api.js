import axios from "axios";

class API {
  axiosInstance = null;

  constructor() {
    /* 
      🚨1 point EXTRA CREDIT 🚨 👉🏿 get the baseURL from the environment
      https://create-react-app.dev/docs/adding-custom-environment-variables/
    */
    const axiosInstance = axios.create({
      baseURL: "https://kwitter-api.herokuapp.com/",
      timeout: 30000,
      headers: { Authorization: `Bearer ${getToken()}` },
    });

    // Add a request interceptor to attach a
    axiosInstance.interceptors.request.use(
      (config) => ({
        ...config,
        headers: {
          ...config.headers,
          Authorization: `Bearer ${getToken()}`,
        },
      }),
      (error) => Promise.reject(error)
    );

    // Add a response interceptor
    axiosInstance.interceptors.response.use(
      ({ data }) => data,
      (error) => Promise.reject(error)
    );

    this.axiosInstance = axiosInstance;
  }

  // AUTH
  async login({ username, password }) {
    try {
      const result = await this.axiosInstance.post("/auth/login", {
        username,
        password,
      });
      return result;
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async logout() {
    try {
      await this.axiosInstance.get("/auth/logout");
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  // USERS
  async getUsers() {
    try {
      return await this.axiosInstance.get("/users");
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  // {
  //   "username": "string",
  //   "displayName": "string",
  //   "password": "string"
  // }
  async register({ username, displayName, password }) {
    try {
      return await this.axiosInstance.post("/users", {
        username,
        displayName,
        password,
      });
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async getUserByUsername(username) {
    try {
      return await this.axiosInstance.get(`/users/${username}`);
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  /**
   *
   * @param {String} username
   * @param {Object} data is the shape of the user
   * data = {
      "username": "string",
      "displayName": "string",
      "password": "string"
    }
   */
  async updateUserByUsername(username, data) {
    try {
      return await this.axiosInstance.patch(`/users/${username}`, data);
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async deleteUserByUsername(username) {
    try {
      return await this.axiosInstance.delete(`/users/${username}`);
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async getUserPictureByUsername(username) {
    try {
      return await this.axiosInstance.get(`/users/${username}/picture`);
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async updateUserPictureByUsername(username, photoData) {
    try {
      const formData = new FormData();
      formData.append("picture", photoData);
      return await this.axiosInstance.put(
        `/users/${username}/picture`,
        formData
      );
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  // MESSAGES
  async getMessages() {
    try {
      return await this.axiosInstance.get(`/messages`);
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async createMessage(text) {
    try {
      return await this.axiosInstance.post(`/messages`, { text });
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async getMessageById(messageId) {
    try {
      return await this.axiosInstance.get(`/messages/${messageId}`);
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async deleteMessageById(messageId) {
    try {
      return await this.axiosInstance.delete(`/messages/${messageId}`);
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  // LIKES
  async likeMessage(messageId) {
    try {
      return await this.axiosInstance.post(`/likes`, {
        messageId: Number.parseInt(messageId, 10),
      });
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }

  async unlikeMessage(likeId) {
    try {
      return await this.axiosInstance.delete(`/likes/${likeId}`);
    } catch (err) {
      helpMeInstructor(err);
      throw err;
    }
  }
}

// WARNING.. do not touch below this line if you want to have a good day =]

function helpMeInstructor(err) {
  console.info(
    `
    Did you hit CORRECT the endpoint?
    Did you send the CORRECT data?
    Did you make the CORRECT kind of request [GET/POST/PATCH/DELETE]?
    Check the Kwitter docs 👉🏿 https://kwitter-api.herokuapp.com/docs/#/
    Check the Axios docs 👉🏿 https://github.com/axios/axios
  `,
    err
  );
}

function getToken() {
  try {
    const storedState = JSON.parse(localStorage.getItem("persist:root"));
    return JSON.parse(storedState.auth).isAuthenticated;
  } catch {
    return "";
  }
}

export default new API();
