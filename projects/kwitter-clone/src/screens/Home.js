import React from "react";
import { Row, Col, Container } from "react-bootstrap";
import {
  LoginFormContainer,
  RegisterFormContainer,
  MenuContainer,
} from "../components";

export const HomeScreen = () => (
  <>
    <MenuContainer />
    <Container>
      <Row>
        <Col>
          <LoginFormContainer />
        </Col>
        <Col>
          <RegisterFormContainer />
        </Col>
      </Row>
    </Container>
  </>
);
