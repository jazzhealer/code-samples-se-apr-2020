import produce from "immer";
import { LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT } from "../actions";

// INITIAL STATE
const INITIAL_STATE = {
  isAuthenticated: "",
  username: "",
  loading: false,
  error: "",
};

export const authReducer = (state = { ...INITIAL_STATE }, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case LOGIN: {
        draft.loading = true;
        break;
      }
      case LOGIN_SUCCESS: {
        draft.isAuthenticated = action.payload.token;
        draft.username = action.payload.username;
        draft.loading = false;
        break;
      }
      case LOGIN_FAILURE: {
        draft.error = action.payload;
        draft.loading = false;
        break;
      }
      case LOGOUT: {
        Object.keys(INITIAL_STATE).forEach((key) => {
          draft[key] = INITIAL_STATE[key];
        });
        break;
      }
      default:
        return draft;
    }
  });

// export const authReducer = (state = { ...INITIAL_STATE }, action) => {
//   switch (action.type) {
//     case LOGIN_FAILURE:
//       return {
//         ...INITIAL_STATE,
//         error: action.payload,
//         loading: false,
//       };
//     case LOGOUT:
//       return {
//         ...INITIAL_STATE,
//       };
//     default:
//       return state;
//   }
// };
