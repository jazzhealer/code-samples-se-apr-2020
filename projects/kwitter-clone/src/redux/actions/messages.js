import api from "../../utils/api";

export const LOAD_MESSAGES = "MESSAGES/LOAD_MESSAGES";
export const LOAD_MESSAGES_SUCCESS = "MESSAGES/LOAD_MESSAGES_SUCCESS";
export const LOAD_MESSAGES_FAILURE = "MESSAGES/LOAD_MESSAGES_FAILURE";
export const CREATE_MESSAGE_SUCCESS = "MESSAGES/CREATE_MESSAGE_SUCCESS";
export const CREATE_MESSAGE_FAILURE = "MESSAGES/CREATE_MESSAGE_FAILURE";
export const DELETE_MESSAGE_SUCCESS = "MESSAGES/DELETE_MESSAGE_SUCCESS";
export const DELETE_MESSAGE_FAILURE = "MESSAGES/CREATE_MESSAGE_FAILURE";
export const TOGGLE_MESSAGE_LIKE_SUCCESS =
  "MESSAGES/TOGGLE_MESSAGE_LIKE_SUCCESS";
export const TOGGLE_MESSAGE_LIKE_FAILURE =
  "MESSAGES/TOGGLE_MESSAGE_LIKE_FAILURE";

const loadMessages = () => async (dispatch, getState) => {
  try {
    dispatch({ type: LOAD_MESSAGES });
    const { messages } = await api.getMessages();
    const { username } = getState().auth;

    dispatch({
      type: LOAD_MESSAGES_SUCCESS,
      payload: messages.map((message) => {
        const isLiked = message.likes.find(
          (like) => like.username === username
        );
        message.isLiked = !!isLiked;
        return message;
      }),
    });
  } catch (err) {
    dispatch({
      type: LOAD_MESSAGES_FAILURE,
      payload: err.message,
    });
  }
};

const createMessage = (text) => async (dispatch, getState) => {
  try {
    const { message } = await api.createMessage(text);
    dispatch({
      type: CREATE_MESSAGE_SUCCESS,
      payload: message,
    });
  } catch (err) {
    dispatch({
      type: CREATE_MESSAGE_FAILURE,
      payload: err.message,
    });
  }
};

const deleteMessage = (id) => async (dispatch, getState) => {
  try {
    await api.deleteMessageById(id);
    dispatch({
      type: DELETE_MESSAGE_SUCCESS,
      payload: id,
    });
  } catch (err) {
    dispatch({
      type: DELETE_MESSAGE_FAILURE,
      payload: err.message,
    });
  }
};

const toggleMessageLike = (messageId) => async (dispatch, getState) => {
  try {
    const message = getState().message.data[messageId];
    const { username } = getState().auth;
    const { isLiked } = message;
    if (isLiked) {
      const likeId = message.likes.find((like) => like.username === username)
        .id;
      await api.unlikeMessage(likeId);
    } else {
      await api.likeMessage(messageId);
    }

    const { message: freshMessage } = await api.getMessageById(messageId);

    dispatch({
      type: TOGGLE_MESSAGE_LIKE_SUCCESS,
      payload: {
        messageId,
        freshMessage,
      },
    });
  } catch (err) {
    dispatch({
      type: TOGGLE_MESSAGE_LIKE_FAILURE,
      payload: err.message,
    });
  }
};

export const actions = {
  loadMessages,
  createMessage,
  deleteMessage,
  toggleMessageLike,
};
