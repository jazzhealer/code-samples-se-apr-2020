const shop = [
  { name: "Peaches", price: 5 },
  { name: "Potatoes", price: 3 },
  { name: "Ice Cream", price: 5 },
  { name: "Face Mask", price: 8 },
  { name: "Beer", price: 10 },
  { name: "PS4", price: 500 },
];

const productNames = shop.map(({ name }) => name);
const isTenDollars = shop.find((item) => item.price === 10);
const expensiveItems = shop.filter((item) => item.price === 3 || item.price === 5);
const shopAndMore = shop.map((item) => Object.assign({}, item));

const person = { name: "Vince", age: 29 };
const vince2 = { age: 500, ...person, likesFood: true };

console.log(person === vince2, vince2);
// shop[0].price = 9999;
// console.log({ shopAndMore });
// for (let i = 0; i < shop.length; i++) {
//   if (shop[i].price > 7) {
//     break;
//   }
//   console.log(shop[i]);
// }
