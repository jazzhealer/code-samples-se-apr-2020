import React from "react";
import { Helmet } from "react-helmet";
import { v4 as generateRandomId } from "uuid";
import { Route, Switch, Link, useHistory, useLocation, useParams, useRouteMatch } from "react-router-dom";
import "./App.css";
import Form from "./Form";
import Listing from "./Listing";

const HomePage = ({ homePage = "Pokemon" }) => {
  const match = useRouteMatch();
  const history = useHistory();
  const location = useLocation();
  const params = useParams();

  console.log("HomePage", { history, location, params, match });

  return <h1>{homePage} Bruh</h1>;
};

// class HomePage extends React.Component {
//   render() {
//     const match = useRouteMatch();
//     const history = useHistory();
//     const location = useLocation();
//     const params = useParams();

//     console.log("HomePage", { history, location, params, match });

//     return <h1>Home Bruh</h1>;
//   }
// }

const NotFoundBruh = (props) => {
  console.log("NotFoundBruh", { props });
  return <h1>404 - Dude wheres my route? ...Bruh</h1>;
};

const Navigation = () => (
  <>
    <nav>
      <ul>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/form">Form Page</Link>
        </li>
        <li>
          <Link to="/listing">Listing Page</Link>
        </li>

        <li>
          <Link to="/area-51">Area 51</Link>
        </li>
      </ul>
    </nav>
  </>
);

export default class App extends React.Component {
  state = {
    users: [{ id: "1234", firstName: "T", lastName: "j", height: "tall" }],
    isToggled: false,
  };

  toggle = () => {
    this.setState((state) => ({ isToggled: !state.isToggled }));
  };

  addUser = (user) => {
    this.setState((state) => ({ users: [...state.users, { ...user, id: generateRandomId() }] }));
  };

  render() {
    const { users } = this.state;
    return (
      <div className="App">
        <Helmet>
          <meta charSet="utf-8" />
          <title>Q2 is Awesome!!</title>
        </Helmet>

        {/* <button onClick={this.toggle}>Toggle Me</button> */}

        <Navigation />

        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route path="/form" render={(p) => <Form {...p} addUser={this.addUser} />} />
          <Route path="/listing" render={(p) => <Listing {...p} users={users} />} />
          <Route path="*">
            <NotFoundBruh />
          </Route>
        </Switch>

        {/* {isToggled ? (
          <TimerA name="Apple" sendMessage={this.sendMessage} />
        ) : (
          <TimerB name="Android" sendMessage={this.sendMessage} />
        )}

        <MessagesDisplay messages={messages} /> */}
      </div>
    );
  }
}
