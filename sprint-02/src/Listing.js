import React from "react";
import { Link, Switch, Route, Redirect, useParams } from "react-router-dom";

const User = ({ users }) => {
  const params = useParams();
  const user = users.find((u) => u.id === params.userId);

  if (!user) {
    return <Redirect to="/listing" />;
  }
  return (
    <h3>
      Hi my name is {user.firstName}
      {user.lastName} and I am {user.height}
    </h3>
  );
};

export default class Listing extends React.Component {
  render() {
    const { users, match } = this.props;
    return (
      <>
        <ol>
          {users.map((u) => (
            <li key={u.id}>
              <Link to={`${match.url}/${u.id}`}>{`${u.firstName} ${u.lastName}`}</Link>
            </li>
          ))}
        </ol>

        <Switch>
          <Route exact path={match.path}>
            <h3>Please Select a user</h3>
          </Route>
          <Route path={`${match.path}/:userId`}>
            <User users={users} />
          </Route>
        </Switch>
      </>
    );
  }
}
