import React from "react";

// THIS IS ANDROID
class TimerB extends React.Component {
  componentDidMount() {
    this.props.sendMessage(`${this.props.name}: componentDidMount`);
  }

  componentWillUnmount() {
    this.props.sendMessage(`${this.props.name}: componentWillUnmount`);
  }

  render() {
    const { name } = this.props;
    return (
      <>
        <p>{name}</p>
        <p>{this.num}</p>
        <button onClick={this.plusOne}>Click me!!</button>
      </>
    );
  }
}

export default TimerB;
