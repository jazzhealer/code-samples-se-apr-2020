import React from "react";

export const MessagesDisplay = ({ messages }) => {
  return (
    <ul>
      {messages.map((m, index) => (
        <li style={{ color: "black" }} key={index}>
          {m}
        </li>
      ))}
    </ul>
  );
};

export default MessagesDisplay;
