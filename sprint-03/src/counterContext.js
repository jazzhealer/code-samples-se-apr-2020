import React, { createContext } from "react";
import useCounter from "./hooks/useCounter";

export const CounterContext = createContext();

export const CounterContextProvider = ({ children }) => {
  const value = useCounter();
  return <CounterContext.Provider value={value}>{children}</CounterContext.Provider>;
};
