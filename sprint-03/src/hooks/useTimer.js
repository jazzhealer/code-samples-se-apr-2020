import { useState, useEffect } from "react";

const useTimer = (currentDate = new Date()) => {
  const [date, setDate] = useState(currentDate);
  const tick = () => setDate(new Date());

  useEffect(() => {
    const timer = setInterval(() => {
      tick();
    }, 1000);

    return () => {
      clearInterval(timer);
    };
  }, []);

  return date;
};

export default useTimer;
