import { COUNTER_INCREMENT, COUNTER_DECREMENT, COUNTER_MULTIPLY } from "../actions/counter";
const initialState = 0;

const counterReducer = (state = initialState, action) => {
  switch (action.type) {
    case COUNTER_INCREMENT:
      return state + 1;
    case COUNTER_DECREMENT:
      return state - 1;
    case COUNTER_MULTIPLY:
      return state * action.payload;
    default:
      return state;
  }
};

export default counterReducer;
