import enhancer from "./enhancer";
import Counter from "./Counter";

export default enhancer(Counter);
