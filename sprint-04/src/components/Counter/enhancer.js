import { connect } from "react-redux";
import { actions } from "../../store/actions/counter";
/*
const mapDispatchToProps = (dispatch) => {
  return {
    increment: () => {
      dispatch(actions.increment());
    },

    decrement: () => {
      dispatch(actions.decrement());
    },

    multiply: () => {
      dispatch(actions.multiply());
    },
  };
};
*/

/*
  This will grab information frmo our redux state and we will
  return an object with the keys that we want
  to the specific parts of the entire redux state that we need
*/
const mapStateToProps = (state) => {
  return {
    count: state.counter,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps);
