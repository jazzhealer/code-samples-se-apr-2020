import loginEnhancer from "./Login.enhancer";
import Login from "./Login";

export default loginEnhancer(Login);
