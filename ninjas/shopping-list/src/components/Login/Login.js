import React from "react";

const Login = ({ login }) => {
  const [name, setName] = React.useState("");

  const loginUser = (ev) => {
    ev.preventDefault();
    login(name);
  };

  return (
    <>
      <h2>Please login below</h2>

      <form onSubmit={loginUser}>
        <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
        <button type="submit">Login Bruh!</button>
      </form>
    </>
  );
};

export default Login;
