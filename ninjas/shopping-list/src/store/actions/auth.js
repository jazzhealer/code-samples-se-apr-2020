export const AUTH_LOGIN = "AUTH_LOGIN";
export const AUTH_LOGOUT = "AUTH_LOGOUT";

const login = (payload) => {
  return {
    type: AUTH_LOGIN,
    payload,
  };
};

const logout = () => {
  return {
    type: AUTH_LOGOUT,
  };
};

export const actions = {
  login,
  logout,
};
